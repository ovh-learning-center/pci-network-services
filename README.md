# OVHCloud : Public Cloud Network Services

Terraform projects used in the official certification program offered by OVHCloud.

The scope of the learning program is network services via OpenStack at OVHCloud.

The topics covered include: 

- vRack and private networks
- Gateways
- Security Groups
- Bastions
- Load Balancers

The aim is to offer an infrastructure progression from a minimal setup (no private cloud) up to a full featured cloud service using an SSL/TLS capable load balancer.

## Basic 
![](images/02.10.schema.png)

## Minimal network
![](images/02.11.schema-vrack.png)

## Gateway
![](images/02.12.schema-gatway.png)

## Secgroups
![](images/03.10.schema-secgroups.png)

## Floating IP
![](images/04.10.schema-floatingip.png)

## Simple Load Balancer
![](images/05.10.schema-lb.png)

## HTTPS Load Balancer
![](images/06.10.schema-lb-cerbot.png)
